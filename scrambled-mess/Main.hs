-- Addresses bonus puzzle at https://www.reddit.com/r/adventofcode/comments/zv4ixy/my_daughter_made_me_my_own_advent_of_code/

import AoC
import Data.List
import Data.Char

main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      text <- readFile dataFileName
      let duplicated = fmap head $ filter ((> 1) . length) $ group text
      putStrLn $ filter isLetterIsh duplicated
      putStrLn $ filter isDigit duplicated

isLetterIsh :: Char -> Bool
isLetterIsh c = (isLetter c) || (c == '-')
