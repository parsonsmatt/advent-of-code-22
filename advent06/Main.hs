-- Writeup at https://work.njae.me.uk/2022/12/06/advent-of-code-2022-day-6/

import AoC
import Data.List

-- test = "mjqjpqmgbljsphdztnvjfqwrcgsmlb"

main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      text <- readFile dataFileName
      -- print $ part1 test
      -- print $ part2 test
      print $ part1 text
      print $ part2 text

part1 :: String -> Int
part1 = interestingPosition 4

part2 :: String -> Int
part2 = interestingPosition 14

interestingPosition :: Int -> String -> Int
interestingPosition n text = n + (fst packetPos)
  where candidates = zip [0..] $ fmap (take n) $ tails text
        packetPos = head $ dropWhile (hasSame . snd) candidates

allDifferent, hasSame :: String -> Bool
allDifferent cs = nub cs == cs
hasSame = not . allDifferent
